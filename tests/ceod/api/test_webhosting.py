import datetime
from unittest.mock import patch

from ceo_common.model import Term
import ceo_common.utils
from tests.utils import create_php_file_for_club, reset_disable_club_conf, create_website_config_for_club


def test_disable_club_sites(
    cfg, client, webhosting_srv, webhosting_srv_resources, new_club_gen,
    new_user_gen, g_admin_ctx, ldap_srv_session,
):
    sites_available_dir = webhosting_srv.sites_available_dir
    term = Term.current()
    clubs_home = cfg.get('clubs_home')
    with patch.object(ceo_common.utils, 'get_current_datetime') as now_mock:
        now_mock.return_value = datetime.datetime.now()
        with new_club_gen() as group, new_user_gen() as user:
            create_php_file_for_club(clubs_home, group.cn)
            user.add_non_member_terms([str(Term.current())])
            group.add_member(user.uid)

            now_mock.return_value = (term + 4).to_datetime()
            status, data = client.post('/api/webhosting/disableclubsites?dry_run=true')
            assert status == 200
            assert data == [group.cn]

            status, data = client.post('/api/webhosting/disableclubsites')
            assert status == 200
            assert data == [group.cn]

            with g_admin_ctx():
                group = ldap_srv_session.get_group(group.cn)
            assert group.members == [user.uid]

            reset_disable_club_conf(webhosting_srv)
            create_website_config_for_club(sites_available_dir, group.cn)

            status, data = client.post('/api/webhosting/disableclubsites?remove_inactive_club_reps=true')
            assert status == 200
            assert data == [group.cn]

            with g_admin_ctx():
                group = ldap_srv_session.get_group(group.cn)
            assert group.members == []
