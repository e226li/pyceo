import ldap3


def test_get_user(client, uwldap_user):
    uid = uwldap_user.uid
    status, data = client.get(f'/api/uwldap/{uid}')
    assert status == 200
    expected = {
        "cn": uwldap_user.cn,
        "given_name": uwldap_user.given_name,
        "mail_local_addresses": uwldap_user.mail_local_addresses,
        "program": uwldap_user.program,
        "sn": uwldap_user.sn,
        "uid": uwldap_user.uid,
    }
    assert data == expected


def test_updateprograms(
        cfg, ldap_conn, client, ldap_user, uwldap_user):
    # sanity check
    assert ldap_user.uid == uwldap_user.uid
    # modify the user's program in UWLDAP
    conn = ldap_conn
    base_dn = cfg.get('uwldap_base')
    dn = f'uid={uwldap_user.uid},{base_dn}'
    changes = {'ou': [(ldap3.MODIFY_REPLACE, ['New Program'])]}
    conn.modify(dn, changes)

    status, data = client.post('/api/uwldap/updateprograms?dry_run=True')
    assert status == 200
    expected = [
        [uwldap_user.uid, uwldap_user.program, 'New Program'],
    ]
    assert data == expected

    # make sure that the user wasn't changed
    status, data = client.get(f'/api/members/{uwldap_user.uid}')
    assert status == 200
    assert data['program'] == ldap_user.program

    status, data = client.post(
        '/api/uwldap/updateprograms', json={'members': ['no_such_user']})
    assert status == 200
    assert data == []

    status, data = client.post('/api/uwldap/updateprograms')
    assert status == 200

    # make sure that the user was changed
    status, data = client.get(f'/api/members/{uwldap_user.uid}')
    assert data['program'] == 'New Program'
