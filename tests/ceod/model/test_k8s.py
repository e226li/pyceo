import json
from unittest.mock import patch


def test_k8s_create_account(k8s_srv):
    # The KubernetesService should really be tested against a live cluster,
    # but that's hard to do in a local environment.
    # So we're just going to make sure that it doesn't crash, at least.
    k8s_srv.create_account('test1')


def test_k8s_get_accounts(k8s_srv):
    class MockProcess:
        def __init__(self, output: str):
            self.stdout = output

    def mock_run(args, check=True, **kwargs):
        return MockProcess(json.dumps({'items': [
            {'metadata': {'name': 'csc-test1'}},
            {'metadata': {'name': 'csc-test2'}},
            {'metadata': {'name': 'kube-system'}},
        ]}))

    expected = ['test1', 'test2']
    with patch.object(k8s_srv, '_run') as run_method:
        run_method.side_effect = mock_run
        assert k8s_srv.get_accounts() == expected
