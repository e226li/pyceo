import pytest

from ceo_common.errors import GroupNotFoundError, UserAlreadyInGroupError, \
    UserNotInGroupError


def test_group_add_to_ldap(simple_group, ldap_srv):
    group = simple_group

    group.add_to_ldap()
    retrieved_group = ldap_srv.get_group(group.cn)
    assert retrieved_group.cn == group.cn

    group.remove_from_ldap()
    with pytest.raises(GroupNotFoundError):
        ldap_srv.get_group(group.cn)


def test_group_members(ldap_group, ldap_srv):
    group = ldap_group

    group.add_member('member1')
    assert group.members == ['member1']
    assert ldap_srv.get_group(group.cn).members == group.members

    with pytest.raises(UserAlreadyInGroupError):
        group.add_member('member1')

    group.add_member('member2')
    assert group.members == ['member1', 'member2']
    assert ldap_srv.get_group(group.cn).members == group.members

    group.remove_member('member1')
    assert group.members == ['member2']
    assert ldap_srv.get_group(group.cn).members == group.members

    with pytest.raises(UserNotInGroupError):
        group.remove_member('member1')

    group.set_members(['member3'])
    assert group.members == ['member3']
    assert ldap_srv.get_group(group.cn).members == group.members


def test_group_to_dict(ldap_group, ldap_user, g_admin_ctx):
    group = ldap_group

    with g_admin_ctx():
        # we need LDAP credentials because to_dict() might make calls
        # to LDAP
        expected = {
            'cn': group.cn,
            'description': group.user_cn,
            'gid_number': group.gid_number,
            'members': [],
        }
        assert group.to_dict() == expected

        group.add_member(ldap_user.uid)
        expected['members'].append({
            'uid': ldap_user.uid,
            'cn': ldap_user.cn,
            'program': ldap_user.program,
        })
        assert group.to_dict() == expected
