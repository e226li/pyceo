from click.testing import CliRunner
from ceo.cli import cli
from ceod.model import User, Group


def test_positions(cli_setup, g_admin_ctx):
    runner = CliRunner()

    # Setup test data
    users = []
    with g_admin_ctx():
        for i in range(5):
            user = User(
                uid=f'test_{i}',
                cn=f'Test {i}',
                given_name='Test',
                sn=str(i),
                program='Math',
                terms=['f2021'],
            )
            user.add_to_ldap()
            users.append(user)
        group = Group(
            cn='exec',
            description='Test Group',
            gid_number=10500,
        )
        group.add_to_ldap()

    result = runner.invoke(cli, [
        'positions', 'set',
        '--president', 'test_0',
        '--vice-president', 'test_1',
        '--sysadmin', 'test_2',
        '--secretary', 'test_3',
        '--webmaster', 'test_4',
    ], input='y\n')

    assert result.exit_code == 0
    assert result.output == '''
The positions will be updated:
president:      test_0
vice-president: test_1
sysadmin:       test_2
secretary:      test_3
webmaster:      test_4
treasurer:
cro:
librarian:
imapd:
offsck:
Do you want to continue? [y/N]: y
Update positions in LDAP... Done
Update executive group in LDAP... Done
Subscribe to mailing lists... Done
Transaction successfully completed.
'''[1:]

    result = runner.invoke(cli, ['positions', 'get'])
    assert result.exit_code == 0
    assert result.output == '''
president:      test_0
secretary:      test_3
sysadmin:       test_2
vice-president: test_1
webmaster:      test_4
'''[1:]

    # Cleanup test data
    with g_admin_ctx():
        for user in users:
            user.remove_from_ldap()
        group.remove_from_ldap()


def test_positions_multiple_users(cli_setup, g_admin_ctx):
    runner = CliRunner()

    # Setup test data
    users = []
    with g_admin_ctx():
        for i in range(5):
            user = User(
                uid=f'test_{i}',
                cn=f'Test {i}',
                given_name='Test',
                sn=str(i),
                program='Math',
                terms=['w2023'],
            )
            user.add_to_ldap()
            users.append(user)
        group = Group(
            cn='exec',
            description='Test Group',
            gid_number=10500,
        )
        group.add_to_ldap()

    result = runner.invoke(cli, [
        'positions', 'set',
        '--president', 'test_0',
        '--vice-president', 'test_1,test_2',
        '--sysadmin', 'test_2',
        '--secretary', 'test_3, test_4, test_2',
    ], input='y\n')

    assert result.exit_code == 0
    assert result.output == '''
The positions will be updated:
president:      test_0
vice-president: test_1, test_2
sysadmin:       test_2
secretary:      test_3, test_4, test_2
treasurer:
cro:
librarian:
imapd:
webmaster:
offsck:
Do you want to continue? [y/N]: y
Update positions in LDAP... Done
Update executive group in LDAP... Done
Subscribe to mailing lists... Done
Transaction successfully completed.
'''[1:]

    result = runner.invoke(cli, ['positions', 'get'])
    assert result.exit_code == 0
    assert result.output == '''
president:      test_0
secretary:      test_2, test_3, test_4
sysadmin:       test_2
vice-president: test_1, test_2
'''[1:]

    # Cleanup test data
    with g_admin_ctx():
        for user in users:
            user.remove_from_ldap()
        group.remove_from_ldap()
