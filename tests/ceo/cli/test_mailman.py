from click.testing import CliRunner

from ceo.cli import cli


def test_mailman_subscribe(cli_setup, mock_mailman_server):
    mock_mailman_server.clear()
    runner = CliRunner()
    result = runner.invoke(cli, ['mailman', 'subscribe', 'test_1', 'exec'], input='y\n')
    expected = (
        "Are you sure you want to subscribe test_1 to exec? [y/N]: y\n"
        "Done.\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    result = runner.invoke(cli, ['mailman', 'unsubscribe', 'test_1', 'exec'], input='y\n')
    expected = (
        "Are you sure you want to unsubscribe test_1 from exec? [y/N]: y\n"
        "Done.\n"
    )
    assert result.exit_code == 0
    assert result.output == expected
