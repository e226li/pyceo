import pytest
import os
import shutil

from click.testing import CliRunner
from ceo.cli import cli

from psycopg2 import connect, OperationalError, ProgrammingError


def psql_attempt_connection(host, username, password):
    con = connect(
        host=host,
        user=username,
        password=password,
    )
    con.autocommit = True
    with con.cursor() as cur:
        cur.execute("SELECT datname FROM pg_database")
        response = cur.fetchall()
        # 3 of the 4 are postgres, template0, template1
        assert len(response) == 4
        with pytest.raises(ProgrammingError):
            cur.execute("CREATE DATABASE new_db")
    con.close()


def test_postgresql(cli_setup, cfg, ldap_user):
    runner = CliRunner()

    username = ldap_user.uid
    os.makedirs(ldap_user.home_directory)
    host = cfg.get("postgresql_host")
    info_file_path = os.path.join(ldap_user.home_directory, "ceo-postgresql-info")
    assert not os.path.isfile(info_file_path)

    # create database for user
    result = runner.invoke(cli, ['postgresql', 'create', username], input='y\n')
    assert result.exit_code == 0
    assert os.path.isfile(info_file_path)

    response_arr = result.output.split()
    passwd = response_arr[response_arr.index("Password:") + 1]
    with open(info_file_path, 'r') as file:
        old_info = file.read()

    expected = f"""Are you sure you want to create a PostgreSQL database for {username}? [y/N]: y
PostgreSQL database created.
Connection Information:

Database: {username}
Username: {username}
Password: {passwd}
Host: {host}

These settings have been written to {info_file_path}.
"""

    assert result.output == expected
    psql_attempt_connection(host, username, passwd)

    # perform password reset for user
    result = runner.invoke(cli, ['postgresql', 'pwreset', username], input="y\n")
    assert result.exit_code == 0

    response_arr = result.output.split()
    new_passwd = response_arr[response_arr.index("Password:") + 1]
    with open(info_file_path, 'r') as file:
        new_info = file.read()

    assert new_passwd != passwd
    assert old_info != new_info
    psql_attempt_connection(host, username, new_passwd)

    # delete database and file
    result = runner.invoke(cli, ['postgresql', 'delete', username], input="y\n")
    assert result.exit_code == 0

    # user should be deleted
    with pytest.raises(OperationalError):
        psql_attempt_connection(host, username, passwd)

    shutil.rmtree(ldap_user.home_directory)
