#!/bin/bash

set -ex

. .drone/common.sh

# set FQDN in /etc/hosts
add_fqdn_to_hosts "$(get_ip_addr $(hostname))" phosphoric-acid
add_fqdn_to_hosts "$(get_ip_addr auth1)" auth1
add_fqdn_to_hosts "$(get_ip_addr coffee)" coffee
# mail container doesn't run in CI
if [ -z "$CI" ]; then
    add_fqdn_to_hosts $(get_ip_addr mail) mail
fi

auth_setup phosphoric-acid

# initialize the skel directory
shopt -s dotglob
mkdir -p /users/skel
cp /etc/skel/* /users/skel/

# create directories for users
for user in ctdalek regular1 exec1; do
    mkdir -p /users/$user
    chown $user:$user /users/$user
done

sync_with coffee
if [ -z "$CI" ]; then
    sync_with mail
fi
