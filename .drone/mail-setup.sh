#!/bin/bash

set -ex

. .drone/common.sh

# set FQDN in /etc/hosts
add_fqdn_to_hosts $(get_ip_addr $(hostname)) mail
add_fqdn_to_hosts $(get_ip_addr auth1) auth1

. venv/bin/activate
python -m tests.MockMailmanServer &
python -m tests.MockSMTPServer &
python -m tests.MockCloudStackServer &
python -m tests.MockHarborServer &

auth_setup mail

# for the VHostManager
mkdir -p /run/ceod/member-vhosts

# sync with phosphoric-acid
nc -l 0.0.0.0 9000 &
