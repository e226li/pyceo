# Documentation
## OpenAPI
We are using [OpenAPI 3.0](https://swagger.io/docs/specification/about/) to
document the REST API for ceod, and [Redoc](https://github.com/Redocly/redoc)
to generate HTML documentation from the OpenAPI file.

First, make sure you have Node.js and npm installed. Then, install the Redoc CLI:
```sh
npm install -g redoc-cli
```
After you make changes to the openapi.yaml file, make sure to regenerate the HTML:
```sh
redoc-cli bundle openapi.yaml
```
You can now view the [redoc-static.html](./redoc-static.html) file from your browser.

## Man pages
We are using [scdoc](https://git.sr.ht/~sircmpwn/scdoc) to generate our man pages.
You can view the pages from your terminal like so:
```sh
scdoc < ceo.1.scd > ceo.1
man ./ceo.1
```
