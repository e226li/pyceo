from abc import ABC, abstractmethod
from typing import Union

import requests


class StreamResponseHandler(ABC):
    """
    An abstract class to handle stream responses from the server.
    The CLI and TUI should implement a child class.
    """

    @abstractmethod
    def handle_non_200(self, resp: requests.Response):
        """Handle a non-200 response."""

    @abstractmethod
    def begin(self):
        """Begin the transaction."""

    @abstractmethod
    def handle_aborted(self, err_msg: str):
        """Handle an aborted transaction."""

    @abstractmethod
    def handle_completed(self):
        """Handle a completed transaction."""

    @abstractmethod
    def handle_successful_operation(self):
        """Handle a successful operation."""

    @abstractmethod
    def handle_failed_operation(self, err_msg: Union[str, None]):
        """Handle a failed operation."""

    @abstractmethod
    def handle_skipped_operation(self):
        """Handle a skipped operation."""

    @abstractmethod
    def handle_unrecognized_operation(self, operation: str):
        """Handle an unrecognized operation."""
