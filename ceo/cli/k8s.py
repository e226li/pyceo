import os
import traceback

import click

from ..utils import http_post
from .utils import handle_sync_response


@click.group(short_help='Manage your CSC Kubernetes resources')
def k8s():
    pass


@k8s.group(short_help='Manage your CSC Kubernetes account')
def account():
    pass


@account.command(short_help='Obtain a kubeconfig')
def activate():
    kubedir = os.path.join(os.environ['HOME'], '.kube')
    if not os.path.isdir(kubedir):
        os.mkdir(kubedir)
    kubeconfig = os.path.join(kubedir, 'config')
    resp = http_post('/api/cloud/k8s/accounts/create')
    result = handle_sync_response(resp)
    try:
        if os.path.isfile(kubeconfig):
            kubeconfig_bak = os.path.join(kubedir, 'config.bak')
            os.rename(kubeconfig, kubeconfig_bak)
        with open(kubeconfig, 'w') as fo:
            fo.write(result['kubeconfig'])
        os.chmod(kubeconfig, 0o600)
    except Exception:
        click.echo(traceback.format_exc())
        click.echo("We weren't able to write the kubeconfig file, so here it is.")
        click.echo("Make sure to paste this into your ~/.kube/config.")
        click.echo()
        click.echo(result['kubeconfig'])
        return
    click.echo("Congratulations! You have a new kubeconfig in ~/.kube/config.")
    click.echo("Run `kubectl cluster-info` to make sure everything is working.")
