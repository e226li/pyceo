from .ConfirmationView import ConfirmationView


class AddGroupConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"A new group '{self.model.name}' will be created."
        ]
        self.set_lines(lines)
