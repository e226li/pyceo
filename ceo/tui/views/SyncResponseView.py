from .PlainTextView import PlainTextView


class SyncResponseView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = ['Request successfully completed.']
        self.set_lines(
            lines,
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
