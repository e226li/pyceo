import urwid

from .ColumnView import ColumnView


class AddGroupView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.name_edit = urwid.Edit()
        self.description_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Name:', align='right'),
                self.name_edit
            ),
            (
                urwid.Text('Description:', align='right'),
                self.description_edit
            )
        ]
        self.set_rows(rows)
