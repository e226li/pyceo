import urwid

from .ColumnView import ColumnView


class ColumnResponseView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)

    def set_pairs(self, pairs, right_col_weight=1):
        for i, (left, right) in enumerate(pairs):
            if type(right) is list:
                pairs[i] = (left, ','.join(map(str, right)))
            else:
                pairs[i] = (left, str(right))
        rows = [
            (
                urwid.Text(
                    left + ':' if left != '' else '',
                    align='right'
                ),
                urwid.Text(right)
            )
            for left, right in pairs
        ]
        self.set_rows(
            rows,
            right_col_weight=right_col_weight,
            disable_cols=True,
            no_back_button=True,
            on_next=self.controller.get_next_menu_callback('Welcome')
        )
