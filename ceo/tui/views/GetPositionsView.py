from zope import component
import urwid

from .ColumnView import ColumnView
from .position_names import position_names
from ceo_common.interfaces import IConfig


class GetPositionsView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.position_fields = {}
        cfg = component.getUtility(IConfig)
        avail = cfg.get('positions_available')
        rows = []
        for pos in avail:
            name = position_names[pos]
            field = urwid.Text('...')
            self.position_fields[pos] = field
            self.model.positions[pos] = ''
            rows.append((urwid.Text(name, align='right'), field))
        self.set_rows(rows, disable_cols=True, no_next_button=True)

    def activate(self):
        self.controller.lookup_positions_async()
        super().activate()

    def update_fields(self):
        for pos, field in self.position_fields.items():
            field.set_text(self.model.positions[pos])
