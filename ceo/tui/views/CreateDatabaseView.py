import urwid

from .ColumnView import ColumnView


class CreateDatabaseView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        db_types_group = []
        rows = [
            (
                urwid.Text('Database type:', align='right'),
                urwid.RadioButton(
                    db_types_group,
                    'MySQL',
                    on_state_change=self.controller.on_db_type_changed,
                    user_data='mysql'
                )
            ),
            (
                urwid.Divider(),
                urwid.RadioButton(
                    db_types_group,
                    'PostgreSQL',
                    on_state_change=self.controller.on_db_type_changed,
                    user_data='postgresql'
                )
            ),
        ]
        self.set_rows(rows)
