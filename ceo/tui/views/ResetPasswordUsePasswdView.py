from .PlainTextView import PlainTextView


class ResetPasswordUsePasswdView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            "This is a utility for syscom members to reset other members' passwords.",
            '',
            'To reset your own password, just run `passwd`.'
        ]
        super().set_lines(
            lines,
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
