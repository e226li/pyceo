from .ConfirmationView import ConfirmationView


class ChangeLoginShellConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"{self.model.username}'s login shell will be set to {self.model.login_shell}."
        ]
        self.set_lines(lines)
