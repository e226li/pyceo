import urwid

from .ColumnView import ColumnView


class RenewUserView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        membership_types_group = []
        self.username_edit = urwid.Edit()
        self.num_terms_edit = urwid.IntEdit(default=1)
        rows = [
            (
                urwid.Text('Membership type:', align='right'),
                urwid.RadioButton(
                    membership_types_group,
                    'General membership ($2)',
                    on_state_change=self.controller.on_membership_type_changed,
                    user_data='general_member'
                )
            ),
            (
                urwid.Divider(),
                urwid.RadioButton(
                    membership_types_group,
                    'Club rep (free)',
                    on_state_change=self.controller.on_membership_type_changed,
                    user_data='club_rep'
                )
            ),
            (
                urwid.Text('Username:', align='right'),
                self.username_edit
            ),
            (
                urwid.Text('Number of terms:', align='right'),
                self.num_terms_edit
            ),
        ]
        self.set_rows(rows, right_col_weight=2)
