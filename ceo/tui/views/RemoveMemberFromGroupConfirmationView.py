from .ConfirmationView import ConfirmationView


class RemoveMemberFromGroupConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"User '{self.model.username}' will be removed from the group '{self.model.name}'."
        ]
        self.set_lines(lines)
