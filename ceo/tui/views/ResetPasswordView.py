import urwid

from .ColumnView import ColumnView


class ResetPasswordView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.username_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Username:', align='right'),
                self.username_edit
            )
        ]
        self.set_rows(rows)
