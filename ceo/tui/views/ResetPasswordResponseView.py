from .PlainTextView import PlainTextView


class ResetPasswordResponseView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"The new password for {self.model.username} is:",
            '',
            self.model.resp_json['password'],
            '',
            'YOU MUST NOW EMAIL THIS PASSWORD TO THE USER.'
        ]
        self.set_lines(
            lines,
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
