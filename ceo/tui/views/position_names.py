position_names = {
    'president': "President",
    'vice-president': "Vice President",
    'treasurer': "Treasurer",
    'secretary': "Secretary",
    'sysadmin': "Sysadmin",
    'cro': "Chief Returning Officer",
    'librarian': "Librarian",
    'imapd': "IMAPD",
    'webmaster': "Web Master",
    'offsck': "Office Manager",
}
