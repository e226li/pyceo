import urwid

from .ColumnView import ColumnView


class SearchGroupView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        # used for query, consider combining Controller.user_name_from_view and Controller.get_group_name_from_view
        self.username_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Query:', align='right'),
                self.username_edit
            )
        ]
        self.set_rows(rows)
