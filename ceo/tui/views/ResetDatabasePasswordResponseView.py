from .PlainTextView import PlainTextView


class ResetDatabasePasswordResponseView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)

    def activate(self):
        self.controller.write_db_creds_to_file()
        db_type = self.model.db_type
        db_type_name = 'MySQL' if db_type == 'mysql' else 'PostgreSQL'
        username = self.model.user_dict['uid']
        password = self.model.password
        filename = self.model.filename
        wrote_to_file = self.model.wrote_to_file
        lines = [
            f'The new {db_type_name} password for {username} is:'
            '',
            password,
            ''
        ]
        if wrote_to_file:
            lines.append(f"The settings in {filename} have been updated.")
        else:
            lines.append(f"We were unable to update the settings in {filename}.")
        self.set_lines(
            lines,
            align='left',
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
        super().activate()
