from .ConfirmationView import ConfirmationView
import ceo.krb_check as krb


class ResetDatabasePasswordConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        db_type = self.model.db_type
        db_type_name = 'MySQL' if db_type == 'mysql' else 'PostgreSQL'
        username = krb.get_username()
        lines = [
            f"The {db_type_name} password for {username} will be reset."
        ]
        self.set_lines(lines)
