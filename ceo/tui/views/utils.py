import urwid


def replace_column_element(columns, idx, elem):
    _, options = columns.contents[idx]
    columns.contents[idx] = elem, options


class CenterButton(urwid.Button):
    def __init__(self, label, on_press=None, user_data=None):
        super().__init__('', on_press, user_data)
        text = urwid.Text(label, align='center')
        text._selectable = True
        replace_column_element(self._w, 1, text)


def decorate_button(button):
    # See the palette in start.py
    return urwid.AttrMap(button, None, focus_map='reversed')


def wrap_in_frame(
    widget,
    title,
    subtitle=None,
    on_back=None,
    on_next=None,
    next_btn=None,
    flash_text=None,
    message_text=None,
):
    back_button_wrapper = urwid.WidgetDisable(urwid.Text(''))
    next_button_wrapper = urwid.WidgetDisable(urwid.Text(''))
    if on_back is not None:
        back_button = CenterButton('Back', on_back)
        back_button_wrapper = decorate_button(back_button)
    if on_next is not None:
        next_button = CenterButton('Next', on_next)
        next_button_wrapper = decorate_button(next_button)
    elif next_btn is not None:
        next_button_wrapper = next_btn
    if on_back is not None or on_next is not None or next_btn is not None:
        footer = urwid.Columns([
            urwid.WidgetDisable(urwid.Text('')),
            back_button_wrapper,
            urwid.WidgetDisable(urwid.Text('')),
            next_button_wrapper,
            urwid.WidgetDisable(urwid.Text('')),
        ])
        footer_height = 1
        if flash_text is not None:
            flash_text = urwid.WidgetDisable(flash_text)
            footer = urwid.Pile([flash_text, footer])
            footer_height = 2
        elif message_text is not None:
            footer = urwid.Pile([message_text, footer])
            footer_height = 7  # ???
        footer_height += 1  # add 1 for the bottom padding
        footer = urwid.Filler(footer, valign='bottom', bottom=1)
        body = urwid.Pile([widget, (footer_height, footer)])
    else:
        body = widget
        if flash_text is not None:
            flash_text = urwid.WidgetDisable(flash_text)
            footer = urwid.Filler(flash_text, valign='bottom', bottom=1)
            body = urwid.Pile([body, (2, footer)])
    header_elems = [urwid.Text(('bold', title), align='center')]
    if subtitle is not None:
        header_elems.append(urwid.Text(subtitle, align='center'))
    else:
        header_elems.append(urwid.Divider())
    header = urwid.Pile(header_elems)
    return urwid.Frame(body, header)
