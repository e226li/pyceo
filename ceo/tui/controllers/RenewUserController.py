from threading import Thread

from ...utils import http_post
from .Controller import Controller
from .SyncRequestController import SyncRequestController
import ceo.term_utils as term_utils
from ceo.tui.views import RenewUserConfirmationView


class RenewUserController(SyncRequestController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def on_membership_type_changed(self, radio_button, new_state, selected_type):
        if new_state:
            self.model.membership_type = selected_type

    def on_next_button_pressed(self, button):
        try:
            username = self.get_username_from_view()
            num_terms = self.get_num_terms_from_view()
        except Controller.InvalidInput:
            return
        self.model.username = username
        self.model.num_terms = num_terms
        self.view.flash_text.set_text('Looking up user...')
        Thread(target=self._get_next_terms).start()

    def _get_next_terms(self):
        try:
            self.model.new_terms = term_utils.get_terms_for_renewal_for_user(
                self.model.username,
                self.model.num_terms,
                self.model.membership_type == 'club_rep',
                self
            )
        except Controller.RequestFailed:
            return

        def target():
            self.view.flash_text.set_text('')
            view = RenewUserConfirmationView(self.model, self, self.app)
            self.switch_to_view(view)

        self.app.run_in_main_loop(target)

    def get_resp(self):
        uid = self.model.username
        body = {'uid': uid}
        if self.model.membership_type == 'club_rep':
            body['non_member_terms'] = self.model.new_terms
        else:
            body['terms'] = self.model.new_terms
        return http_post(f'/api/members/{uid}/renew', json=body)
