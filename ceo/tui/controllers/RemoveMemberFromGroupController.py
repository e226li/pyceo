from .Controller import Controller
from ceod.transactions.groups import RemoveMemberFromGroupTransaction
from .TransactionController import TransactionController
from ceo.tui.models import TransactionModel
from ceo.tui.views import RemoveMemberFromGroupConfirmationView, TransactionView


class RemoveMemberFromGroupController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)

    def on_list_unsubscribe_checkbox_change(self, checkbox, new_state):
        self.model.unsubscribe_from_lists = new_state

    def on_next_button_pressed(self, button):
        try:
            self.model.name = self.get_group_name_from_view()
            self.model.username = self.get_username_from_view()
        except Controller.InvalidInput:
            return
        view = RemoveMemberFromGroupConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)

    def on_confirmation_button_pressed(self, button):
        cn = self.model.name
        uid = self.model.username
        url = f'/api/groups/{cn}/members/{uid}'
        if not self.model.unsubscribe_from_lists:
            url += '?unsubscribe_from_lists=false'
        model = TransactionModel(
            RemoveMemberFromGroupTransaction.operations,
            'DELETE', url
        )
        controller = TransactionController(model, self.app)
        view = TransactionView(model, controller, self.app)
        controller.view = view
        self.switch_to_view(view)
