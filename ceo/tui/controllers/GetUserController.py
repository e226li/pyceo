from ...utils import http_get
from .Controller import Controller
from .SyncRequestController import SyncRequestController
from ceo.tui.views import GetUserResponseView


class GetUserController(SyncRequestController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def get_resp(self):
        return http_get(f'/api/members/{self.model.username}')

    def get_response_view(self):
        return GetUserResponseView(self.model, self, self.app)

    def on_next_button_pressed(self, button):
        try:
            self.model.username = self.get_username_from_view()
        except Controller.InvalidInput:
            return
        self.on_confirmation_button_pressed(button)
