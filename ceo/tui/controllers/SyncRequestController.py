from threading import Thread

from .Controller import Controller
import ceo.tui.utils as tui_utils
from ceo.tui.views import SyncResponseView


class SyncRequestController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)
        self.request_in_progress = False

    def get_resp(self):
        # To be implemented by child classes
        raise NotImplementedError()

    def get_response_view(self):
        return SyncResponseView(self.model, self, self.app)

    def on_confirmation_button_pressed(self, button):
        if self.request_in_progress:
            return
        self.request_in_progress = True
        self.view.flash_text.set_text('Sending request...')

        def main_loop_target():
            self.view.flash_text.set_text('')
            view = self.get_response_view()
            self.switch_to_view(view)

        def thread_target():
            resp = self.get_resp()
            try:
                self.model.resp_json = tui_utils.handle_sync_response(resp, self)
            except Controller.RequestFailed:
                return
            self.app.run_in_main_loop(main_loop_target)

        Thread(target=thread_target).start()
