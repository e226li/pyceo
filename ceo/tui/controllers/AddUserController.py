from threading import Thread

from ...utils import http_get
from .Controller import Controller
from .AddUserTransactionController import AddUserTransactionController
from ceo.tui.models import TransactionModel
from ceo.tui.views import AddUserConfirmationView, TransactionView
from ceo_common.model.Term import get_terms_for_new_user
from ceod.transactions.members import AddMemberTransaction


class AddUserController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)
        self.right_col_idx = 0
        self.prev_searched_username = None

    def on_confirmation_button_pressed(self, button):
        body = {
            'uid': self.model.username,
            'cn': self.model.full_name,
            'given_name': self.model.first_name,
            'sn': self.model.last_name,
        }
        if self.model.program:
            body['program'] = self.model.program
        if self.model.forwarding_address:
            body['forwarding_addresses'] = [self.model.forwarding_address]
        new_terms = get_terms_for_new_user(self.model.num_terms)
        if self.model.membership_type == 'club_rep':
            body['non_member_terms'] = new_terms
        else:
            body['terms'] = new_terms

        model = TransactionModel(
            AddMemberTransaction.operations,
            'POST', '/api/members',
            json=body
        )
        controller = AddUserTransactionController(model, self.app)
        view = TransactionView(model, controller, self.app)
        controller.view = view
        self.switch_to_view(view)

    def on_next_button_pressed(self, button):
        try:
            username = self.get_username_from_view()
            num_terms = self.get_num_terms_from_view()
        except Controller.InvalidInput:
            return
        full_name = self.view.full_name_edit.edit_text
        # TODO: share validation logic between CLI and TUI
        if not full_name:
            self.view.popup('Full name must not be empty')
            return
        self.model.username = username
        self.model.full_name = full_name
        self.model.first_name = self.view.first_name_edit.edit_text
        self.model.last_name = self.view.last_name_edit.edit_text
        self.model.program = self.view.program_edit.edit_text
        self.model.forwarding_address = self.view.forwarding_address_edit.edit_text
        self.model.num_terms = num_terms
        view = AddUserConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)

    def on_membership_type_changed(self, radio_button, new_state, selected_type):
        if new_state:
            self.model.membership_type = selected_type

    def on_row_focus_changed(self):
        _, idx = self.view.listwalker.get_focus()
        old_idx = self.right_col_idx
        self.right_col_idx = idx
        # The username field is the third row, so when
        # idx changes from 2 to 3, this means the user
        # moved from the username field to the next field
        if old_idx == 2 and idx == 3:
            Thread(
                target=self._lookup_user,
                args=(self.view.username_edit.edit_text,)
            ).start()

    def _set_flash_text(self, *args):
        self.view.flash_text.set_text('Looking up user...')

    def _clear_flash_text(self):
        self.view.flash_text.set_text('')

    def _on_lookup_user_success(self):
        self._clear_flash_text()
        self.view.update_fields()

    def _lookup_user(self, username):
        if not username:
            return
        if username == self.prev_searched_username:
            return
        self.prev_searched_username = username
        self.app.run_in_main_loop(self._set_flash_text)
        resp = http_get('/api/uwldap/' + username)
        if not resp.ok:
            self.app.run_in_main_loop(self._clear_flash_text)
            return
        data = resp.json()
        self.model.full_name = data.get('cn', '')
        self.model.first_name = data.get('given_name', '')
        self.model.last_name = data.get('sn', '')
        self.model.program = data.get('program', '')
        self.model.forwarding_address = (data.get('mail_local_addresses') or [''])[0]
        self.app.run_in_main_loop(self._on_lookup_user_success)
