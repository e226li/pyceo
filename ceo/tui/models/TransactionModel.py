class TransactionModel:
    name = 'Transaction'
    title = 'Running transaction'

    def __init__(self, operations, http_verb, req_path, **req_kwargs):
        self.operations = operations
        self.http_verb = http_verb
        self.req_path = req_path
        self.req_kwargs = req_kwargs
