class AddUserModel:
    name = 'AddUser'
    title = 'Add user'

    def __init__(self):
        self.membership_type = 'general_member'
        self.username = ''
        self.full_name = ''
        self.first_name = ''
        self.last_name = ''
        self.program = ''
        self.forwarding_address = ''
        self.num_terms = 1
