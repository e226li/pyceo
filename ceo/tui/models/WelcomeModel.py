from .AddUserModel import AddUserModel
from .RenewUserModel import RenewUserModel
from .GetUserModel import GetUserModel
from .ResetPasswordModel import ResetPasswordModel
from .ChangeLoginShellModel import ChangeLoginShellModel
from .AddGroupModel import AddGroupModel
from .GetGroupModel import GetGroupModel
from .SearchGroupModel import SearchGroupModel
from .AddMemberToGroupModel import AddMemberToGroupModel
from .RemoveMemberFromGroupModel import RemoveMemberFromGroupModel
from .CreateDatabaseModel import CreateDatabaseModel
from .ResetDatabasePasswordModel import ResetDatabasePasswordModel
from .GetPositionsModel import GetPositionsModel
from .SetPositionsModel import SetPositionsModel


class WelcomeModel:
    name = 'Welcome'
    title = 'CSC Electronic Office'

    def __init__(self):
        self.categories = {
            'Members': [
                AddUserModel,
                RenewUserModel,
                GetUserModel,
                ResetPasswordModel,
                ChangeLoginShellModel,
            ],
            'Groups': [
                AddGroupModel,
                GetGroupModel,
                SearchGroupModel,
                AddMemberToGroupModel,
                RemoveMemberFromGroupModel,
            ],
            'Databases': [
                CreateDatabaseModel,
                ResetDatabasePasswordModel,
            ],
            'Positions': [
                GetPositionsModel,
                SetPositionsModel,
            ],
        }
