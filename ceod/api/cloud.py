from flask import Blueprint, request
from zope import component

from .utils import requires_authentication_no_realm, authz_restrict_to_syscom, \
    get_valid_member_or_throw
from ceo_common.interfaces import ICloudStackService, IVHostManager, \
    IKubernetesService, ICloudResourceManager, IContainerRegistryService

bp = Blueprint('cloud', __name__)


@bp.route('/accounts/create', methods=['POST'])
@requires_authentication_no_realm
def create_account(auth_user: str):
    user = get_valid_member_or_throw(auth_user)
    cloudstack_srv = component.getUtility(ICloudStackService)
    cloudstack_srv.create_account(user)
    return {'status': 'OK'}


@bp.route('/accounts/purge', methods=['POST'])
@authz_restrict_to_syscom
def purge_accounts():
    cloud_mgr = component.getUtility(ICloudResourceManager)
    return cloud_mgr.purge_accounts()


@bp.route('/vhosts/<domain>', methods=['PUT'])
@requires_authentication_no_realm
def create_vhost(auth_user: str, domain: str):
    get_valid_member_or_throw(auth_user)
    vhost_mgr = component.getUtility(IVHostManager)
    body = request.get_json(force=True)
    ip_address = body['ip_address']
    vhost_mgr.create_vhost(auth_user, domain, ip_address)
    return {'status': 'OK'}


@bp.route('/vhosts/<domain>', methods=['DELETE'])
@requires_authentication_no_realm
def delete_vhost(auth_user: str, domain: str):
    vhost_mgr = component.getUtility(IVHostManager)
    vhost_mgr.delete_vhost(auth_user, domain)
    return {'status': 'OK'}


@bp.route('/vhosts', methods=['GET'])
@requires_authentication_no_realm
def get_vhosts(auth_user: str):
    vhost_mgr = component.getUtility(IVHostManager)
    vhosts = vhost_mgr.get_vhosts(auth_user)
    return {'vhosts': vhosts}


@bp.route('/k8s/accounts/create', methods=['POST'])
@requires_authentication_no_realm
def create_k8s_account(auth_user: str):
    get_valid_member_or_throw(auth_user)
    k8s_srv = component.getUtility(IKubernetesService)
    kubeconfig = k8s_srv.create_account(auth_user)
    return {
        'status': 'OK',
        'kubeconfig': kubeconfig,
    }


@bp.route('/registry/projects', methods=['POST'])
@requires_authentication_no_realm
def create_registry_project(auth_user: str):
    get_valid_member_or_throw(auth_user)
    reg_srv = component.getUtility(IContainerRegistryService)
    reg_srv.create_project_for_user(auth_user)
    return {'status': 'OK'}
