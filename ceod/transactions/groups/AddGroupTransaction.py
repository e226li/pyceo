from zope import component

from ..AbstractTransaction import AbstractTransaction
from ceo_common.interfaces import ILDAPService
from ceod.model import Group, User


class AddGroupTransaction(AbstractTransaction):
    """Transaction to create a new group."""

    operations = [
        'add_user_to_ldap',
        'add_group_to_ldap',
        'add_sudo_role',
        'create_home_dir',
    ]

    def __init__(
        self,
        cn: str,
        description: str,
    ):
        super().__init__()
        self.cn = cn
        self.description = description
        self.user = None
        self.group = None

    def child_execute_iter(self):
        ldap_srv = component.getUtility(ILDAPService)

        # The user's UID is the group's CN.
        # The user's CN is the group's description.
        user = User(
            uid=self.cn,
            cn=self.description,
            is_club=True,
        )
        self.user = user
        user.add_to_ldap()
        yield 'add_user_to_ldap'

        # For now, we only store the description in the user CN, and not in
        # the group record. We can change this later if desired.
        group = Group(
            cn=self.cn,
            gid_number=user.gid_number,
            user_cn=self.description,
        )
        self.group = group
        group.add_to_ldap()
        yield 'add_group_to_ldap'

        ldap_srv.add_sudo_role(self.cn)
        yield 'add_sudo_role'

        user.create_home_dir()
        yield 'create_home_dir'

        self.finish(group.to_dict())

    def rollback(self):
        ldap_srv = component.getUtility(ILDAPService)

        if 'create_home_dir' in self.finished_operations:
            self.user.delete_home_dir()
        if 'add_sudo_role' in self.finished_operations:
            ldap_srv.remove_sudo_role(self.cn)
        if 'add_group_to_ldap' in self.finished_operations:
            self.group.remove_from_ldap()
        if 'add_user_to_ldap' in self.finished_operations:
            self.user.remove_from_ldap()
