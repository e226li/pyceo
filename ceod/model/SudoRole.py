from zope import component

from .utils import strings_to_bytes
from ceo_common.interfaces import IConfig


class SudoRole:
    """Represents a sudoRole record in LDAP."""

    def __init__(self, uid: str):
        cfg = component.getUtility(IConfig)
        ldap_sudo_base = cfg.get('ldap_sudo_base')

        self.uid = uid
        self.dn = f'cn=%{uid},{ldap_sudo_base}'

    def serialize_for_ldap(self):
        # TODO: use sudoOrder
        data = {
            'objectClass': [
                'top',
                'sudoRole',
            ],
            'cn': '%' + self.uid,
            'sudoUser': '%' + self.uid,
            'sudoHost': 'ALL',
            'sudoCommand': 'ALL',
            'sudoOption': '!authenticate',
            'sudoRunAsUser': self.uid,
        }
        return strings_to_bytes(data)
