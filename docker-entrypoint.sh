#!/bin/sh -e

if ! [ -d venv ]; then
    echo "You need to create the virtualenv first!" >&2
    exit 1
fi

host="$1"
[ -x ".drone/$host-setup.sh" ] && ".drone/$host-setup.sh"

if [ "$host" = auth1 ]; then
    exec sleep infinity
else
    . venv/bin/activate
    exec .drone/supervise.sh flask run -h 0.0.0.0 -p 9987
fi
