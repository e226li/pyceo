#!/usr/bin/env python3
"""
This is a script which unsubscribes expired members from csc-general.

GSSAPI is used for SPNEGO authentication, so make sure to run `kinit` first.
Also, make sure to run this script from the top-level of the git directory
(see the sys.path hack below).
"""
import os
import sys

import ldap3
from zope import component

sys.path.append(os.getcwd())
from ceo_common.errors import UserNotSubscribedError
from ceo_common.interfaces import IConfig, IHTTPClient
from ceo_common.model import Config, HTTPClient, RemoteMailmanService

# modify as necessary
CONFIG_FILE = '/etc/csc/ceod.ini'
NEW_MEMBER_LIST = 'csc-general'

cfg = Config(CONFIG_FILE)
component.provideUtility(cfg, IConfig)
http_client = HTTPClient()
component.provideUtility(http_client, IHTTPClient)
mailman_srv = RemoteMailmanService()
LDAP_URI = cfg.get('ldap_server_url')
LDAP_MEMBERS_BASE = cfg.get('ldap_users_base')

conn = ldap3.Connection(LDAP_URI, auto_bind=True, raise_exceptions=True)
conn.search(LDAP_MEMBERS_BASE, '(shadowExpire=1)', attributes=['uid'])
total_unsubscribed = 0
for entry in conn.entries:
    uid = entry.uid.value
    try:
        mailman_srv.unsubscribe(uid, NEW_MEMBER_LIST)
        print(f'Unsubscribed {uid}')
        total_unsubscribed += 1
    except UserNotSubscribedError:
        print(f'{uid} is already unsubscribed')
print(f'Total unsubscribed: {total_unsubscribed}')
