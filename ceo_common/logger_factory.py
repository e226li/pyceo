import logging

__ALL__ = ['logger_factory']


def logger_factory(name: str) -> logging.Logger:
    logger = logging.getLogger(name)
    if logger.hasHandlers():
        # already initialized
        return logger
    logger.setLevel(logging.DEBUG)
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    log_handler.setFormatter(logging.Formatter('%(levelname)s %(name)s: %(message)s'))
    logger.addHandler(log_handler)
    return logger
