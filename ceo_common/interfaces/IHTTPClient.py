from zope.interface import Interface


class IHTTPClient(Interface):
    """A helper class for HTTP requests to ceod."""

    def request(host: str, path: str, method: str, **kwargs):
        """
        Make an HTTP request.
        **kwargs are passed to requests.request().
        """

    def get(host: str, path: str, **kwargs):
        """Make a GET request."""

    def post(host: str, path: str, **kwargs):
        """Make a POST request."""

    def patch(host: str, path: str, **kwargs):
        """Make a PATCH request."""

    def delete(host: str, path: str, **kwargs):
        """Make a DELETE request."""
