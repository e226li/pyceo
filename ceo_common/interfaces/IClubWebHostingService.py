from typing import List

from zope.interface import Interface


class IClubWebHostingService(Interface):
    """
    Performs operations on the configuration files of club websites hosted
    by the CSC.
    """

    def begin_transaction():
        """
        Performs any steps necessary to query the website config files.
        This must be called BEFORE any of the methods below, using a context
        expression like so:

        with club_site_mgr.begin_transaction():
            club_site_mgr.disable_club_site('club1')
            ...
            club_site_mgr.commit()
        """

    def commit():
        """
        Writes the config file changes to disk. This must be called at the
        end of the context expression.
        """

    def disable_club_site(club_name: str):
        """
        Disables the site for the club. Note that commit() must still be
        called to commit this change.
        """

    def disable_sites_for_inactive_clubs(
        dry_run: bool = False,
        remove_inactive_club_reps: bool = False,
    ) -> List[str]:
        """
        Disables sites for inactive clubs. If remove_inactive_club_reps is set
        to True, then inactive club reps will be removed from club groups.
        The list of clubs whose sites were disabled (or would have been
        disabled, if dry_run is True) is returned.
        """
