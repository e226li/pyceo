from typing import Dict

from zope.interface import Interface


class ICloudResourceManager(Interface):
    """Manages multiple cloud resources for each user."""

    def purge_accounts() -> Dict:
        """
        Delete cloud resources for expired CSC accounts.
        A warning message will be emailed to users one week before their
        cloud account is deleted.
        Another message will be emailed to the users after their cloud account
        has been deleted.
        """
