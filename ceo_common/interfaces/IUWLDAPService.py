from typing import List, Union

from zope.interface import Interface


class IUWLDAPService(Interface):
    """Represents the UW LDAP database."""

    def get_user(username: str):
        """
        Return the LDAP record for the given user, or
        None if no such record exists.

        :rtype: Union[UWLDAPRecord, None]
        """

    def get_programs_for_users(usernames: List[str]) -> List[Union[str, None]]:
        """
        Return the programs for the given users from UWLDAP.
        If no record or program is found for a user, their entry in
        the returned list will be None.
        Example:
            get_programs_for_users(['user1', 'user2', 'user3'])
            -> ['program1', None, 'program2']
        """
