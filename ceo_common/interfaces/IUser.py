from typing import List, Dict

from zope.interface import Interface, Attribute


class IUser(Interface):
    """
    Represents a Unix user.

    There are four types of Unix users in the CSC LDAP:
    1. Members
    2. Club reps
    3. Clubs
    4. System accounts (e.g. syscom, sysadmin, progcom, exec, git, www)

    Members can become club reps and vice versa. The last term registration
    of a user determines if they are a member or club rep.
    """

    # LDAP attributes
    uid = Attribute('user identifier')
    cn = Attribute('full name')
    given_name = Attribute('first name')
    sn = Attribute('last name')
    login_shell = Attribute('login shell')
    uid_number = Attribute('uid number')
    gid_number = Attribute('gid number')
    home_directory = Attribute('home directory')
    program = Attribute('academic program')
    position = Attribute('executive position')
    terms = Attribute('list of terms for which this person was a member')
    non_member_terms = Attribute('list of terms for which this person was '
                                 'a club rep')
    mail_local_addresses = Attribute('email aliases')
    is_club_rep = Attribute('whether this user is a club rep or not')
    shadowExpire = Attribute('whether the user is marked as expired')

    # Non-LDAP attributes
    ldap3_entry = Attribute('cached ldap3.Entry instance for this user')

    def get_forwarding_addresses(self) -> List[str]:
        """Get the forwarding addresses for this user."""

    def set_forwarding_addresses(self, addresses: List[str]):
        """Set the forwarding addresses for this user."""

    def is_club() -> bool:
        """
        Returns True if this is the Unix user for a club.
        Returns False if this is the Unix user for a member.
        """

    def is_member_or_club_rep() -> bool:
        """
        Returns True iff this user has the 'member' objectClass.
        """

    def is_member() -> bool:
        """Returns True iff this user is a member."""

    def add_to_ldap():
        """
        Add a new record to LDAP for this user.
        A new UID number and GID number will be created.
        """

    def add_to_kerberos(password: str):
        """Add a new Kerberos principal for this user."""

    def remove_from_kerberos():
        """Remove this user from Kerberos."""

    def add_terms(terms: List[str]):
        """Add member terms for this user."""

    def add_non_member_terms(terms: List[str]):
        """Add non-member terms for this user."""

    def set_positions(self, positions: List[str]):
        """Set the positions for this user."""

    def change_password(password: str):
        """Replace this user's password."""

    def replace_login_shell(login_shell: str):
        """Replace this user's login shell."""

    def create_home_dir():
        """Create a new home directory for this user."""

    def delete_home_dir():
        """Delete this user's home dir."""

    def subscribe_to_mailing_list(mailing_list: str):
        """Subscribe this user to the mailing list."""

    def unsubscribe_from_mailing_list(mailing_list: str):
        """Unsubscribe this user from the mailing list."""

    def to_dict(get_forwarding_addresses: bool = False) -> Dict:
        """
        Serialize this user into a dict.
        If get_forwarding_addresses is True, the forwarding addresses
        for the user will also be returned, if present.
        """

    def membership_is_valid() -> bool:
        """Returns True iff the user's has a non-expired membership."""
